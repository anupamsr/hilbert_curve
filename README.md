# Hilbert curve
Draw hilbert curves as an image.

# How to run
```
$ python main.py
```

![Output](hilbert.png)

# Documentation
direction is an ```int``` type representing the 4 directions in a 2D plane - left, top, right, bottom
```python
from PIL import Image

colors = ("blue", "orange", "green", "red", "violet", "brown", "pink", "gray", "yellow", "cyan")
img = Image.new("RGB", (1000, 1000), color="white")
draw_hilbert(image=img,     # Image object from Pillow
             direction=0,   # 0 means the curve starts from the left
             depth=4,       # How many levels deep we should go while dividing the image into quadrants
             colors=c,      # All the different colors that the line segments will be drawn in
             width=50)      # Width of the line segment
img.show()
```
