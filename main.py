from PIL import Image
from typing import List
import argparse
import sys
import hilbert


def parse_arguments(argv: List[str]):
    parser = argparse.ArgumentParser(description="Specify parameters for calculation")
    parser.add_argument(
        "--direction",
        type=int,
        default=0,
        required=False,
        help="0 means the curve starts from the left",
    )
    parser.add_argument(
        "--depth",
        type=int,
        default=4,
        required=False,
        help="How many levels deep we should go while dividing the image into quadrants",
    )
    parser.add_argument(
        "--image-x",
        type=int,
        default=1000,
        required=False,
        help="Width of the image in pixels",
    )
    parser.add_argument(
        "--image-y",
        type=int,
        default=1000,
        required=False,
        help="Height of the image in pixels",
    )
    parser.add_argument(
        "--width",
        type=int,
        default=50,
        required=False,
        help="Width of the line segment",
    )
    parser.add_argument(
        "--background-color",
        type=str,
        default="white",
        required=False,
        help="Background color of the image",
    )
    parser.add_argument(
        "--foreground-colors",
        nargs="+",
        # default = ["red", "green", "blue"]
        default=[
            "blue",
            "orange",
            "green",
            "red",
            "violet",
            "brown",
            "pink",
            "gray",
            "yellow",
            "cyan",
        ],
        required=False,
        help="All the different colors that the line segments will be drawn in",
    )
    args = parser.parse_args(argv)
    image_size = (args.image_x, args.image_y)
    return (
        args.direction,
        args.depth,
        image_size,
        args.width,
        args.background_color,
        args.foreground_colors,
    )


def main(argv):
    (
        direction,
        depth,
        image_size,
        width,
        background_color,
        foreground_colors,
    ) = parse_arguments(argv)
    img = Image.new("RGB", image_size, color=background_color)
    hilbert.draw_hilbert(
        image=img,
        direction=direction,
        depth=depth,
        colors=foreground_colors,
        width=width,
    )
    img.show()


if __name__ == "__main__":
    main(sys.argv[1:])
