from typing import Tuple

from PIL import Image, ImageDraw


def draw_hilbert(image: Image, direction: int, depth: int, colors: Tuple[str], width: int):
    draw = ImageDraw.Draw(image)
    color_idx = direction % len(colors)
    direction = direction % 4
    x, y = image.size
    if depth < 1:
        if direction == 0:
            draw.line([(0, y / 2), (x / 2, y / 2)], fill=colors[color_idx], width=width)
        elif direction == 1:
            draw.line([(x / 2, 0), (x / 2, y / 2)], fill=colors[color_idx], width=width)
        elif direction == 2:
            draw.line([(x, y / 2), (x / 2, y / 2)], fill=colors[color_idx], width=width)
        else:
            draw.line([(x / 2, y), (x / 2, y / 2)], fill=colors[color_idx], width=width)
    else:
        draw_quadrants(draw, direction, depth - 1, colors, color_idx, width, (True, True),
                       ((0, 0), (x, y)))


def draw_quadrants(draw: ImageDraw, direction: int, depth: int, colors: Tuple[str], color_idx: int,
                   width: int, connection_lines: Tuple[bool, bool],
                   screen: Tuple[Tuple[int, int], Tuple[int, int]]):
    direction = direction % 4
    color_idx = color_idx % len(colors)
    top_left = screen[0]
    bottom_right = screen[1]
    if depth == 0:
        dx = abs((bottom_right[0] - top_left[0]) / 4)
        dy = abs((bottom_right[1] - top_left[1]) / 4)
        p1 = (top_left[0] + dx, top_left[1] + dy)
        p2 = (bottom_right[0] - dx, top_left[1] + dy)
        p3 = (bottom_right[0] - dx, bottom_right[1] - dy)
        p4 = (top_left[0] + dx, bottom_right[1] - dy)
        if connection_lines[0]:
            p0 = (top_left[0], top_left[1] + dy)
        else:
            p0 = (top_left[0] + dx, top_left[1])
        if connection_lines[1]:
            p5 = (top_left[0], bottom_right[1] - dy)
        else:
            p5 = (top_left[0] + dx, bottom_right[1])

        if direction == 1:
            p1, p2, p3, p4 = p2, p3, p4, p1
            if connection_lines[0]:
                p0 = (bottom_right[0] - dx, top_left[1])
            else:
                p0 = (bottom_right[0], top_left[1] + dy)
            if connection_lines[1]:
                p5 = (top_left[0] + dx, top_left[1])
            else:
                p5 = (top_left[0], top_left[1] + dy)
        elif direction == 2:
            p1, p2, p3, p4 = p3, p4, p1, p2
            if connection_lines[0]:
                p0 = (bottom_right[0], bottom_right[1] - dy)
            else:
                p0 = (bottom_right[0] - dx, bottom_right[1])
            if connection_lines[1]:
                p5 = (bottom_right[0], top_left[1] + dy)
            else:
                p5 = (bottom_right[0] - dx, top_left[1])
        elif direction == 3:
            p1, p2, p3, p4 = p4, p1, p2, p3
            if connection_lines[0]:
                p0 = (top_left[0] + dx, bottom_right[1])
            else:
                p0 = (top_left[0], bottom_right[1] - dy)
            if connection_lines[1]:
                p5 = (bottom_right[0] - dx, bottom_right[1])
            else:
                p5 = (bottom_right[0], bottom_right[1] - dy)

        draw.line([p0, p1, p2, p3, p4, p5], fill=colors[color_idx], width=width, joint="curve")

    else:
        mid = ((top_left[0] + bottom_right[0]) / 2, (top_left[1] + bottom_right[1]) / 2)
        quad_top_left = (top_left, mid)
        quad_top_right = ((mid[0], top_left[1]), (bottom_right[0], mid[1]))
        quad_bottom_left = ((top_left[0], mid[1]), (mid[0], bottom_right[1]))
        quad_bottom_right = (mid, bottom_right)
        connection_lines_top_left = (False, not connection_lines[0])
        connection_lines_top_right = (True, False)
        connection_lines_bottom_left = (False, True)
        connection_lines_bottom_right = (not connection_lines[1], False)

        if direction == 1:
            quad_top_left, quad_top_right, quad_bottom_right, quad_bottom_left = \
                quad_top_right, quad_bottom_right, quad_bottom_left, quad_top_left
        elif direction == 2:
            quad_top_left, quad_top_right, quad_bottom_right, quad_bottom_left = \
                quad_bottom_right, quad_bottom_left, quad_top_left, quad_top_right
        elif direction == 3:
            quad_top_left, quad_top_right, quad_bottom_right, quad_bottom_left = \
                quad_bottom_left, quad_top_left, quad_top_right, quad_bottom_right

        draw_quadrants(draw, direction + 1, depth - 1, colors, color_idx, width,
                       connection_lines_top_left, quad_top_left)
        draw_quadrants(draw, direction, depth - 1, colors, color_idx + 1, width,
                       connection_lines_top_right, quad_top_right)
        draw_quadrants(draw, direction, depth - 1, colors, color_idx + 2, width,
                       connection_lines_bottom_left, quad_bottom_right)
        draw_quadrants(draw, direction + 3, depth - 1, colors, color_idx + 3, width,
                       connection_lines_bottom_right, quad_bottom_left)
